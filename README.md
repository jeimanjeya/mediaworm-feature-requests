# MediaWorm #

### Feature Requests ###
Hello there. Welcome to the feature requests repository. You maybe wondering why a separate repository for only feature requests? Well, in this way it becomes easy for us to keep a track of things. 

### How do I get set up? ###
You can make a feature request by clicking on the **[Issues](https://bitbucket.org/jeimanjeya/mediaworm-feature-requests/issues?status=new&status=open)** tab and then clicking on the New Issue button. There will be an option to add Issue Kind (bug, enhancement, proposal & task), just choose **proposal** option and we will have a look at your request. 

### How long does it take for the requested feature to be implemented? ###
There is no definite time limit. Can take up to a couple of days/weeks/months. Depends on the priority and importance of the requested feature.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### License ###

```
#!python
Copyright 2016 Jeiman Jeya (@jeimanjeya, https://bitbucket.org/jeimanjeya)
  
  Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

```